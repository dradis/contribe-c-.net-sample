﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Foodie.ViewModels;
using System.Collections;
using Foodie.Helpers;

/**
Anti tampering POST/GET Validation.
If decorated method parameter implements IHiddenValidator, this attr. takes param fields covered by tampering protection, calculates token and compares with token from userspace
If tokens dont match, redirects to previous page.
**/
namespace Foodie.Attributes {
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public sealed class ValidateSecureHiddenInputsAttribute : ActionFilterAttribute {

        private List<string> properties = null;
        private const string DefaultRedirectURL = "/TableSearch";

        public override void OnActionExecuting(ActionExecutingContext actionContext) {
            var actionParam = actionContext.ActionParameters[actionContext.ActionParameters.Keys.First()];
            
            if (actionParam is IHiddenValidator) {
                properties = ((Foodie.ViewModels.IHiddenValidator)actionParam).GetListOfHiddenFields();
            }

            if ( properties != null) {
                try {
                    if( actionContext.RequestContext.HttpContext.Request.HttpMethod == "POST") {
                        ValidateRequestPOST(actionContext);
                    } else if(actionContext.RequestContext.HttpContext.Request.HttpMethod == "GET") {
                        ValidateRequestGET(actionContext);
                    }
                    
                } catch(HttpSecureHiddenInputException e) {
                    var url = actionContext.HttpContext.Request.ServerVariables["HTTP_REFERER"];
                    if( url == null) {
                        url = DefaultRedirectURL;
                    }
                    actionContext.Result = new RedirectResult(url);
                }                
            }
        }

        private void ValidateRequestPOST(ActionExecutingContext actionContext) {
            properties.ForEach(s => Validate(actionContext, s));
        }

        private static void Validate(ActionExecutingContext filterContext, string property) {

            var protectedValue = filterContext.HttpContext.Request.Form[string.Format("__{0}Token", property)];

            if( protectedValue == null) {
                throw new HttpSecureHiddenInputException("Missing field");
            }

            var originalValue = filterContext.HttpContext.Request.Form[property];

            var fieldValid = HMACCalculator.IsPOSTFieldValid(
                protectedValue,
                originalValue,
                HMACCalculator.GenerateDefaultHashKey()
            );
 
            if (!fieldValid) {
                throw new HttpSecureHiddenInputException("A required security token was not supplied or was invalid.");
            }

        }

        private void ValidateRequestGET(ActionExecutingContext actionContext) {
            var calculatedToken = "";
            foreach (var property in properties) {
                var fieldValue = actionContext.RequestContext.HttpContext.Request[property];
                if (fieldValue != null) {
                    calculatedToken += HttpUtility.UrlEncode(fieldValue.ToString());
                }
            }

            calculatedToken = HMACCalculator.CalculateHashGET(calculatedToken);

            var passedToken = actionContext.RequestContext.HttpContext.Request["token"];
            if (passedToken == null || passedToken != calculatedToken) {
                throw new HttpSecureHiddenInputException("Invalid GET token");
            }
        }
    }

    public class HttpSecureHiddenInputException : Exception {
        public HttpSecureHiddenInputException(string message) : base(message) {
        }
    }
}