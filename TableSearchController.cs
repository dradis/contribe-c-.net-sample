﻿using Foodie.Attributes;
using Foodie.DAL;
using Foodie.Models;
using Foodie.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace Foodie.Controllers{

    public class TableSearchController : TableAbstractController{
        private TableSearchContext Model = new TableSearchContext();

        //View Constants
        public const string ViewTableSearch           = "TableSearch";
        public const string ViewTableSearchResult     = "TableSearchResult";
        public const string ViewTableSearchRestaurant = "TableSearchRestaurant";
        public const string ViewDefault               = ViewTableSearch;

        //Action Constants        
        public const string ActionSearch     = "Search";
        public const string ActionLocation   = "Location";
        public const string ActionRestaurant = "Restaurant";

        //Temp dictionary keys
        public const string TempDefaultPageNotification = "DefaultPageNotification";
        public const string TempLocationSearchResults   = "LocationSearchResults";
        public const string TempSearchRestaurantData    = "SearchRestaurantData";

        [HttpGet]
        public async Task<ActionResult> Index(){
            //Default dev. search data
            TableSearchRequestViewModel search = new TableSearchRequestViewModel{
                Destination = "Dubrovnik",
                Date = DateTime.Parse("2015-12-30"),
                NumberOfPersons = 2,
                Time = 12,
                PromoCode = "EXPENSIVE",
                DistributionID = (int)DistributionChannel.PredefinedChannel.MainSite,
                FeaturedLocations = await Model.GetFeaturedLocations(),
                Lang = GetSessionLanguage() 
            };
            return View(ViewTableSearch, search);
        }

        [HttpGet]
        public async Task<ActionResult> GetLocations([Bind(Include = "Location")] LocationsRequest request) {
            if(ModelState.IsValid) {
                var locations = await Model.getMatchingLocations(request);
                return Json(locations.ToArray(),JsonRequestBehavior.AllowGet);
            }
            return null;
        }

        [HttpGet]
        [ValidateSecureHiddenInputs]
        [OutputCache(Duration=360, VaryByParam="LocationID;LocationType;DistributionID;Lang", Location=OutputCacheLocation.Server, NoStore=false)]
        public async Task<ActionResult> Location([Bind(Include ="LocationID,LocationType,DistributionID,Lang")] TableLocationSearchRequestViewModel request) {
            if( request.LocationID > 0 ) {
                
                request.IsLocation = true;
                if( request.LocationType == LocationsResponse.LocationType.Restaurant) {

                    var restaurant = await Model.FindMatchingRestaurantByIDorThrow(request);
                    if( restaurant.IsNoMatchingRestaurants() ) {
                        SetTempDataValue( ResourceEn.TableSearchNoRestaurantFound, TempDefaultPageNotification);
                    } else {
                        return View(ViewTableSearchRestaurant, restaurant);    
                    }
                        
                } else if( request.LocationType == LocationsResponse.LocationType.Location ) {

                    var restaurants = await Model.FindMatchingRestaurantsByLocationOrThrow( request );
                    if( restaurants.IsNoMatchingRestaurants()) {
                        SetTempDataValue( ResourceEn.TableSearchNoRestaurantFound, TempDefaultPageNotification);
                    } else {
                        SetTempDataValue( restaurants, TempLocationSearchResults);
                        return View(ViewTableSearchResult, restaurants);
                    }

                }

            }

            return RedirectToAction(ActionDefault);
        }

        [HttpGet]
        [ValidateSecureHiddenInputs]
        [OutputCache(Duration = 360, VaryByParam = "Destination;Date;Time;NumberOfPersons;DistributionID;PromoCode", Location = OutputCacheLocation.Server, NoStore = false)]
        public async Task<ActionResult> Search([Bind(Include = "Destination,Date,Time,NumberOfPersons,DistributionID,PromoCode")] TableSearchRequestViewModel search){
            if (ModelState.IsValid){

                if (search.LocationType == LocationsResponse.LocationType.Location || search.LocationType == LocationsResponse.LocationType.Geo){
                    return await SearchByLocation(search);
                }
                else if (search.LocationType == LocationsResponse.LocationType.Restaurant){
                    return await SearchByRestaurant(search);
                }
            }
            return RedirectToAction(ActionDefault);
        }

        protected async Task<ActionResult> SearchByLocation(TableSearchRequestViewModel request){

            var searchResults = await Model.FindMatchingRestaurants(request);
            if (searchResults.IsNoMatchingRestaurants()){
                SetTempDataValue(ResourceEn.TableSearchNoRestaurantFound, TempDefaultPageNotification);
                return RedirectToAction(ActionDefault);
            }
            else{
                SetSessionDataValue(searchResults.GetBookingRequestIDs(), SessionBookingRequestsDictionary);
                SetTempDataValue(searchResults, TempLocationSearchResults);
                return View(ViewTableSearchResult, searchResults);
            }
        }

        [HttpGet]
        [ValidateSecureHiddenInputs]
        public async Task<ActionResult> FilterResults([Bind(Include ="CousineTypes,PriceFrom,PriceTo,DistanceFrom,DistanceTo,SearchRequest")] FilterSearchResultsRequestViewModel request) {

            if( request.SearchRequest.IsLocation && request.SearchRequest.LocationID > 0 || 
                (ModelState.IsValid && request.SearchRequest.IsLocation == false)
               ) {

                TableSearchResultsViewModel searchResults = null;
                try {
                    searchResults = (TableSearchResultsViewModel)GetTempDataValue(TempLocationSearchResults);
                } catch( InvalidStateDataException e ) {
                    if( request.SearchRequest.IsLocation ) {
                        searchResults = await Model.FindMatchingRestaurantsByLocationOrThrow( request.SearchRequest );
                    } else {
                        searchResults = await Model.FindMatchingRestaurants(request.SearchRequest);
                    }
                }

                if( searchResults.IsNoMatchingRestaurants() ) {
                    //No rezults?
                } else {
                    var filteredResults = Model.FilterRestaurants(request, searchResults);
                    return View(ViewTableSearchResult, filteredResults);
                }

            }
            return RedirectToAction(ActionDefault);
        }

        protected async Task<ActionResult> SearchByRestaurant(TableSearchRequestViewModel request ) {
            try {
                var restaurantData = await Model.FindRestaurant(request);
                //Register Booking requests
                RegisterBookingRequests( restaurantData.GetBookingRequestIDs() );
                //Store data for Redirect
                SetTempDataValue(restaurantData, TempSearchRestaurantData);
                return RedirectToAction(ActionRestaurant, request);
            } catch( Exception e) {
                return RedirectToAction(ActionDefault);
            }
        }

        public async Task<ActionResult> Restaurant(TableSearchRequestViewModel request) {
            try {
                var restaurantData = (TableSearchRestaurantResultViewModel)GetTempDataValue(TempSearchRestaurantData);
                
                foreach( var bookingRequest in restaurantData.BookingRequestData.BookingRequests.Values) {
                    bookingRequest.ForEach( b => b.View = BookingRequestViewModel.ViewType.Restaurant );
                }
                return View(ViewTableSearchRestaurant, restaurantData);
            } catch( InvalidStateDataException e ) {
                return await Search(request);
            }
        }

    }
}