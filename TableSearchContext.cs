﻿using Foodie.Models;
using Foodie.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Foodie.DAL{
    /**
    Table search model
        - Restaurant search
        - Location search
        - Compares availablity and restaurant sale policy to find free tables matching search criteria
    */
    public class TableSearchContext: TableContextAbstract{
        //Max distance between customer and restaurant
        private const int MaxMeterDistance = 50000;

        //Returns a collection of featured locations
        internal async Task<ICollection<FeaturedLocation>> GetFeaturedLocations(){
            var locations = await FeaturedLocations
                .Select( l => l)
                .Include( l => l.Pictures)
                .Include( l => l.Location )
                .ToListAsync();
            return locations;
        }

        //Returns list of locations matching given request
        internal async Task<List<LocationsResponse>> getMatchingLocations(LocationsRequest request){
            var locations = await Locations
                .Where( l => l.LocationName.Contains(request.Location))
                .Select( l => new { l.LocationName, l.ID })
                .ToListAsync();

            var restaurants = await Restaurants
                .Where( r => r.RestorauntName.Contains(request.Location ))
                .Select( r => new {r.RestorauntName, r.ID })
                .ToListAsync();

            var list = new List<LocationsResponse>();

            foreach( var location in locations ) {
                list.Add(new LocationsResponse {
                    Location = location.LocationName,
                    Type = LocationsResponse.LocationType.Location,
                    LocationID = location.ID
                });
            }
            foreach( var restaurant in restaurants ) {
                list.Add(new LocationsResponse {
                    Location = restaurant.RestorauntName,
                    Type = LocationsResponse.LocationType.Restaurant,
                    LocationID = restaurant.ID
                });
            }

            return list;
        }

        //Returns matching restaurant VM
        internal async Task<TableSearchRestaurantResultViewModel> FindMatchingRestaurantByIDorThrow(TableSearchRequestViewModel request){
            var restaurant = Restaurants
                    .Where(r => r.ID == request.LocationID && r.Active)
                    .Select(r => new MatchingRestaurant{
                        Restaurant = r
                    });

            var result = await restaurant.ToListAsync();

            if (!result.Any()){
                throw new TableBookingNoMatchingRestaurantsException();
            }

            return new TableSearchRestaurantResultViewModel{
                BookingRequestData = new TableSearchResultViewModel(),
                SearchRequest = request,
                RestaurantData = new TableSearchResult{
                    MatchingRestaurants = restaurant.ToList()
                }
            };
        }

        //Returns restaurants at given location
        internal async Task<TableSearchResultsViewModel> FindMatchingRestaurantsByLocationOrThrow(TableSearchRequestViewModel request){
            var restaurants = await Restaurants
                .Where( 
                    r => r.Active == true && r.Locations.Where( location => location.ID == request.LocationID).Any() 
                )
                .Select( restaurant => new TableSearchResultViewModel {
                    BookableRestaurant = restaurant,
                    Lang = request.Lang
                })
                .ToListAsync();

            if( !restaurants.Any() ) {
                throw new TableBookingNoMatchingRestaurantsException();
            }
            restaurants.ForEach( r => r.DistributionID = request.DistributionID);

            return new TableSearchResultsViewModel {
                Results = restaurants,
                SearchRequest = request,
                ResultFilter = new FilterSearchResultsRequestViewModel()
            };
        }

        //Returns available restaurants matching search criteria
        internal async Task<TableSearchResultsViewModel> FindMatchingRestaurants(TableSearchRequestViewModel search){

            var result = await GetRestorauntsMatchingSearch(search);
            TableSearchResultsViewModel freeTableSearchResult = null;
            if (result.MatchingRestaurants.Any()){
                freeTableSearchResult = await GetAvailableTimeslotsForMatchingRestaurants(search, result);
            }
            else{
                freeTableSearchResult = GetEmptyRestaurantSearchResult(search);
            }

            return freeTableSearchResult;
        }

        //Filters restaurants based on provided criteria
        internal TableSearchResultsViewModel FilterRestaurants(FilterSearchResultsRequestViewModel request, TableSearchResultsViewModel searchResults){
            var filteredResults = new TableSearchResultsViewModel {
                SearchRequest = request.SearchRequest,
                Results = new List<TableSearchResultViewModel>(),
                ResultFilter = request
            };
            foreach( var restaurantData in searchResults.Results) {
                var matchesFilter = false;
                //Price Range & Cuisine
                if( (request.PriceFrom <= restaurantData.BookableRestaurant.AveragePrice &&  restaurantData.BookableRestaurant.AveragePrice <= request.PriceTo) || 
                    (request.PriceFrom == 0 && request.PriceTo == 0)
                    ) {

                    if( request.CousineTypes != null) {
                        var restaurantCousines = restaurantData.BookableRestaurant.Cousines.Select( c => c.Type );
                        if( restaurantCousines.Intersect( request.CousineTypes).Any()) {
                            matchesFilter = true;
                        }
                    } else {
                        matchesFilter = true;
                    }
                }

                //Distance
                if( matchesFilter && 
                    !(request.DistanceFrom <= restaurantData.DistanceKm.GetValueOrDefault() && 
                    restaurantData.DistanceKm.GetValueOrDefault() <= request.DistanceTo) ) {
                    matchesFilter = false;
                }

                if( matchesFilter ) {
                    filteredResults.Results.Add(restaurantData);
                }

            }
            return filteredResults;
        }

        internal async Task<TableSearchRestaurantResultViewModel> FindRestaurant(TableSearchRequestViewModel searchRequest){
            var matchingRestaurant = await GetRestorauntsMatchingSearch(searchRequest);
            TableSearchRestaurantResultViewModel restaurantSearchResult = null;
            if (matchingRestaurant.MatchingRestaurants.Any()){
                var availableBookingSlots = await GetAvailableTimeslotsForMatchingRestaurants(searchRequest, matchingRestaurant);

                matchingRestaurant.MatchingRestaurants.First().Restaurant.Pictures.Count();
                matchingRestaurant.MatchingRestaurants.First().Restaurant.Descriptions.Count();
                matchingRestaurant.MatchingRestaurants.First().Restaurant.Locations.Count();

                if (availableBookingSlots.Results.Any()){
                    var offers = availableBookingSlots.Results.First().BookingRequests.Values
                        .SelectMany(model => model)
                        .GroupBy(model => model.Offer);

                    foreach (var offer in offers){
                        if (offer.Key != null){
                            offer.Key.Name.Count();
                            offer.Key.Terms.Count();
                        }
                    }
                }

                restaurantSearchResult = new TableSearchRestaurantResultViewModel{
                    RestaurantData = matchingRestaurant,
                    BookingRequestData = availableBookingSlots.Results.Any() ? availableBookingSlots.Results.First() : null,
                    SearchRequest = searchRequest
                };

            }
            else{
                restaurantSearchResult = new TableSearchRestaurantResultViewModel{
                    RestaurantData = matchingRestaurant,
                    BookingRequestData = null,
                    SearchRequest = searchRequest
                };
            }
            return restaurantSearchResult;
        }

        //Returns VM representing restaurants that match user search params, does not check availability
        private async Task<TableSearchResult> GetRestorauntsMatchingSearch(TableSearchRequestViewModel search){
            var targetDatetime = new DateTime(search.Date.Year, search.Date.Month, search.Date.Day, search.Time, 0, 0);
            var now = DateTime.UtcNow; //TODO: Time offseting

            DbGeography clientLocation = null;
            if (search.LocationType == LocationsResponse.LocationType.Geo){
                clientLocation = DbGeography.FromText(String.Format("POINT({0} {1})", search.Latitude, search.Longitude));
            }

            //TODO Bookings around Now
            var restaurants = Restaurants
            //Filter restaurants based on LocationType (ie. One specific restaurant if LocationID = Rest.id, or all restaurants at given LocationID )
            .Where(
                restaurant =>
                    (
                        (search.LocationType == LocationsResponse.LocationType.Restaurant && restaurant.ID == search.LocationID) ||
                        (search.LocationType == LocationsResponse.LocationType.Location && restaurant.Locations.Where(l => l.ID == search.LocationID).Any()) ||
                        (search.LocationType == LocationsResponse.LocationType.Geo && restaurant.Location.Distance(clientLocation) <= MaxMeterDistance)
                    )
                    &&
                    (restaurant.MaxPersonsPerBooking >= search.NumberOfPersons) &&
                    (restaurant.Active == true)
            )
            .Include(r => r.B2BBlacklist) 
            .Include(r => r.B2BWhitelist) 
            //Inner Join active Distribution channels - only valid distribution channels for this restaurant allowed
            .Join(
                RestaurantChannels,
                r => new { RestaurantID = r.ID, DistributionChannelID = search.DistributionID },
                rChannel => new { RestaurantID = rChannel.RestaurantID, DistributionChannelID = rChannel.DistributionChannelID },
                (r, rChannel) => r
            )
            //Inner Join tables - Dont want tableless restaurant
            .Join(
                Tables,
                r => r.ID,
                t => t.RestaurantID,
                (r, t) => new {
                    Restaurant = r,
                    Table = t
                }
            )
            //Purge tables that dont fit occupancy requirements
            .Where(restaurantTable =>
                    restaurantTable.Table.MinPersons <= search.NumberOfPersons && search.NumberOfPersons <= restaurantTable.Table.MaxPersons
            )
            //Left join Table offers
            .GroupJoin(
                TableOffers,
                restaurant => restaurant.Restaurant.ID,
                offer => offer.RestaurantID,
                (restaurant, offer) => new{
                    Restaurant = restaurant.Restaurant,
                    Table = restaurant.Table,
                    Offers = offer.DefaultIfEmpty()
                        //Purge offers that dont match search requirements
                        .Where(
                            toffer =>
                                    toffer.StartDate <= targetDatetime &&
                                    targetDatetime <= toffer.EndDate &&
                                    toffer.StartTime <= search.Time &&
                                    search.Time <= toffer.EndTime &&
                                    toffer.Active &&
                                    DbFunctions.AddHours(targetDatetime, -toffer.EarlyBooking) <= now &&
                                    now <= DbFunctions.AddHours(targetDatetime, -toffer.ReleasePeriod)
                        )
                        //Purge offers for unsuitable tables
                        .Join(
                            TableOfferTables,
                            toffer => new { TableID = restaurant.Table.ID, TableOfferID = toffer.ID },
                            otables => new { TableID = otables.TableID, TableOfferID = otables.TableOfferID },
                            (toffer, otables) => toffer
                        )

                }
            )
            //Check restaurant policy, only in case there are no offers => Offer policy overrides Restaurant policy
            .Where(
                restaurant => restaurant.Offers.Count() > 0 ||
                                ((DbFunctions.AddHours(targetDatetime, -restaurant.Restaurant.EarlyBooking) <= now) &&
                                (now <= DbFunctions.AddHours(targetDatetime, -restaurant.Restaurant.ReleasePeriod)))
            );
            //Cache all matching restaurants here
            var matchingRestaurants = await restaurants.ToListAsync();

            //B2B?
            var b2bCode = new B2BCode();
            if (search.PromoCode != null && matchingRestaurants.Count > 0){
                var b2bCodes = await B2BCodes
                    .Where(code => code.PromoCode == search.PromoCode && code.Active)
                    .Include(code => code.Blacklist)
                    .Include(code => code.Whitelist)
                    .Include(code => code.Descriptions)
                    .ToListAsync();
                b2bCode = b2bCodes.Count() > 0 ? b2bCodes.First() : null;

                if (b2bCode != null){
                    var b2bRestaurants = matchingRestaurants.Where(
                        r => r.Restaurant.BookableB2B &&
                            //Check that restoraunt supports this B2BCode
                            (
                                (r.Restaurant.B2BType == B2BCode.B2BType.FreeForAll) ||
                                (r.Restaurant.B2BType == B2BCode.B2BType.FreeForAllExcept && (r.Restaurant.B2BBlacklist == null || r.Restaurant.B2BBlacklist.Where(code => code.ApplicationUserID == b2bCode.ApplicationUserID).Count() == 0)) ||
                                (r.Restaurant.B2BType == B2BCode.B2BType.Whitelist && r.Restaurant.B2BWhitelist != null && r.Restaurant.B2BWhitelist.Where(code => code.ApplicationUserID == b2bCode.ApplicationUserID).Count() > 0)
                            )
                                &&
                            //Check that B2B code supports this restoraunt
                            (
                                (b2bCode.Type == B2BCode.B2BType.FreeForAll) ||
                                (b2bCode.Type == B2BCode.B2BType.FreeForAllExcept && (b2bCode.Blacklist == null || b2bCode.Blacklist.Where(code => code.RestaurantID == r.Restaurant.ID).Count() == 0)) ||
                                (b2bCode.Type == B2BCode.B2BType.Whitelist && b2bCode.Whitelist != null && b2bCode.Whitelist.Where(code => code.RestaurantID == r.Restaurant.ID).Count() > 0)
                            )
                    ).ToList();

                    //There are matching restaurants for this promotion
                    //Replace original list with b2blist
                    if (b2bRestaurants.Count() > 0){
                        matchingRestaurants = b2bRestaurants;
                    }
                    else{
                        b2bCode = null;
                    }
                }
            }

            TableSearchResult searchResult = new TableSearchResult();
            searchResult.B2BCode = b2bCode;
            searchResult.UsingB2B = search.PromoCode != null ? true : false;
            searchResult.B2BValid = searchResult.UsingB2B && b2bCode != null;

            matchingRestaurants.ForEach(r => searchResult.MatchingRestaurants.Add(new MatchingRestaurant{
                Restaurant = r.Restaurant,
                Offers = r.Offers,
                Table = r.Table
            }));

            return searchResult;
        }

        //Returns -+1h availability period for given restaurants
        private async Task<TableSearchResultsViewModel> GetAvailableTimeslotsForMatchingRestaurants(TableSearchRequestViewModel search, TableSearchResult result){
            if (!result.MatchingRestaurants.Any()){
                throw new TableBookingNoMatchingRestaurantsException();
            }

            List<String> tableQueries = new List<String>();
            Dictionary<int, List<FieldNameData>> restaurantTableSlots = new Dictionary<int, List<FieldNameData>>();
            List<TableSearchResultViewModel> freeTableSearchResult = new List<TableSearchResultViewModel>();
            var targetDatetime = new DateTime(search.Date.Year, search.Date.Month, search.Date.Day, search.Time, 0, 0);
            var targetDay = targetDatetime.ToString("yyyy-MM-dd");

            foreach (var matchingRestaurant in result.MatchingRestaurants){

                var startTimeMinus1hr = targetDatetime.AddHours(-1);
                var tableSlotParamsMinus1hr = new FieldNameData{
                    StartTime = startTimeMinus1hr,
                    FieldNames = GenerateFieldName(startTimeMinus1hr, 30, matchingRestaurant.Restaurant.MealDuration, "SlotAvailable", " > 0 ")
                };

                var startTimeMinus05hr = targetDatetime.AddHours(-0.5);
                var tableSlotParamsMinus05hr = new FieldNameData{
                    StartTime = startTimeMinus05hr,
                    FieldNames = GenerateFieldName(startTimeMinus05hr, 30, matchingRestaurant.Restaurant.MealDuration, "SlotAvailable", " > 0 ")
                };

                var tableSlotParams = new FieldNameData{
                    StartTime = targetDatetime,
                    FieldNames = GenerateFieldName(targetDatetime, 30, matchingRestaurant.Restaurant.MealDuration, "SlotAvailable", " > 0 ")
                };

                var startTimePlus05hr = targetDatetime.AddHours(0.5);
                var tableSlotParamsPlus05hr = new FieldNameData{
                    StartTime = startTimePlus05hr,
                    FieldNames = GenerateFieldName(startTimePlus05hr, 30, matchingRestaurant.Restaurant.MealDuration, "SlotAvailable", " > 0 ")
                };

                var startTimePlus1hr = targetDatetime.AddHours(1);
                var tableSlotParamsPlus1hr = new FieldNameData{
                    StartTime = startTimePlus1hr,
                    FieldNames = GenerateFieldName(targetDatetime.AddHours(1), 30, matchingRestaurant.Restaurant.MealDuration, "SlotAvailable", " > 0 ")
                };

                String tableSlots = String.Format("( ({0}) OR ({1}) OR ({2}) OR ({3}) OR ({4}) )",
                        String.Join(" AND ", tableSlotParamsMinus1hr.FieldNames.ToArray()),
                        String.Join(" AND ", tableSlotParamsMinus05hr.FieldNames.ToArray()),
                        String.Join(" AND ", tableSlotParams.FieldNames.ToArray()),
                        String.Join(" AND ", tableSlotParamsPlus05hr.FieldNames.ToArray()),
                        String.Join(" AND ", tableSlotParamsPlus1hr.FieldNames.ToArray())
                );
                tableQueries.Add(String.Format("( TableID = {0} AND Day = '{1}' AND {2} )", matchingRestaurant.Table.ID, targetDay, tableSlots));
                //Add to dictionary, so we can later reuse for booking link creation

                restaurantTableSlots.Add(matchingRestaurant.Table.ID, new List<FieldNameData> {
                    tableSlotParamsMinus1hr,
                    tableSlotParamsMinus05hr,
                    tableSlotParams,
                    tableSlotParamsPlus05hr,
                    tableSlotParamsPlus1hr
                });

            }

            //TODO: Only timeslot columns instead of (*)
            var results = await Database.SqlQuery<Availability>("SELECT * FROM Availability WHERE " + String.Join(" OR ", tableQueries.ToArray())).ToListAsync();

            if (results.Count() > 0){
                foreach (var freeTableDay in results){
                    foreach (var matchingRestaurant in result.MatchingRestaurants){

                        if (matchingRestaurant.Table.ID == freeTableDay.TableID){

                            var tableBookingRequests = GenerateBookingRequestsFromAvailabilitySlots(
                                freeTableDay,
                                matchingRestaurant,
                                restaurantTableSlots[matchingRestaurant.Table.ID],
                                search,
                                result.UsingB2B && result.B2BValid ? result.B2BCode.PromoCode : ""
                            );

                            double? distance = null;
                            if (search.LocationType == LocationsResponse.LocationType.Geo){
                                distance = DbGeography.FromText(String.Format("POINT({0} {1})", search.Latitude, search.Longitude)).Distance(matchingRestaurant.Restaurant.Location) / 1000;
                            }

                            freeTableSearchResult.Add(new TableSearchResultViewModel{
                                BookableRestaurant = matchingRestaurant.Restaurant,
                                BookingRequests = matchingRestaurant.Restaurant.BookableOnline ? tableBookingRequests : null,
                                B2BCode = result.B2BCode,
                                B2BCodeValid = result.B2BValid,
                                UsingB2B = result.UsingB2B,
                                DistributionID = search.DistributionID,
                                DistanceKm = distance
                            });
                        }
                    }
                }
            }

            return new TableSearchResultsViewModel{
                Results = freeTableSearchResult,
                SearchRequest = search,
                ResultFilter = new FilterSearchResultsRequestViewModel()
            };
        }

        private Dictionary<string, List<BookingRequestViewModel>> GenerateBookingRequestsFromAvailabilitySlots(
            Availability freeTableDay, 
            MatchingRestaurant restaurant, 
            List<FieldNameData> restaurantTableSlots, 
            TableSearchRequestViewModel searchRequest,
            string promoCode = null
            ) {
            
            Dictionary<string, List<BookingRequestViewModel>> bookingRequests = new Dictionary<string, List<BookingRequestViewModel>>();
            foreach (var tableSlots in restaurantTableSlots) {
                var slotFree = true;
                
                foreach( var availTableSlot in tableSlots.FieldNames) {
                    var availableTableCount = Int32.Parse(freeTableDay.GetType().GetProperty(availTableSlot.Replace(" > 0","").Trim()).GetValue(freeTableDay).ToString());
                    if(availableTableCount <= 0) {
                        slotFree = false;
                        break;
                    }
                }
                //Generate booking link for slot
                if (slotFree) {
                    var slotHourMin = tableSlots.StartTime.ToString("HH:mm");
                    List<BookingRequestViewModel> links = new List<BookingRequestViewModel>();
                    bookingRequests.Add(slotHourMin, links);

                    if ( restaurant.Offers != null) {
                        var slotHour = Int32.Parse(tableSlots.StartTime.ToString("HH"));
                        //Add offers if they exists
                        foreach ( var offer in restaurant.Offers) {
                            if( offer.StartTime <= slotHour && slotHour <= offer.EndTime) {
                                links.Add(new BookingRequestViewModel {
                                    RestaurantID = restaurant.Restaurant.ID,
                                    DistributionID = searchRequest.DistributionID,
                                    TableID = freeTableDay.TableID,
                                    NumberOfPersons = searchRequest.NumberOfPersons,
                                    TargetDateTime = tableSlots.StartTime,
                                    PromoCode = promoCode ?? "",
                                    Timeslot = slotHourMin,
                                    Offer = offer,
                                    TableOfferID = offer.ID
                                });
                            }
                        }
                    }
                    //Add normal table booking request
                    links.Add(new BookingRequestViewModel{
                        RestaurantID = restaurant.Restaurant.ID,
                        TableID = freeTableDay.TableID,
                        NumberOfPersons = searchRequest.NumberOfPersons,
                        TargetDateTime = tableSlots.StartTime,
                        PromoCode = promoCode ?? "",
                        Timeslot = slotHourMin,
                        DistributionID = searchRequest.DistributionID
                    });
                }
            }
            return bookingRequests;
        }

        private TableSearchResultsViewModel GetEmptyRestaurantSearchResult(TableSearchRequestViewModel search) {
            return new TableSearchResultsViewModel {
                Results = new List<TableSearchResultViewModel>(),
                SearchRequest = search,
                ResultFilter = new FilterSearchResultsRequestViewModel()
            };
        }

        private class FieldNameData {
            public DateTime StartTime { get; set; }
            public List<String> FieldNames { get; set; }
        }
    }

}